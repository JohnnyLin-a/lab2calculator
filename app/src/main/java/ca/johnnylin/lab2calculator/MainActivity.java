package ca.johnnylin.lab2calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private LoanCalculator lc = new LoanCalculator();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculate (View view) {
        if (!(((EditText)findViewById(R.id.interestYears)).getText()+"").equalsIgnoreCase("") &&
                !(((EditText)findViewById(R.id.loan)).getText()+"").equalsIgnoreCase("") &&
                !(((EditText)findViewById(R.id.interestRate)).getText()+"").equalsIgnoreCase("")) {
            int years = Integer.parseInt(((EditText) findViewById(R.id.interestYears)).getText() + "");
            double amount = Double.parseDouble(((EditText) findViewById(R.id.loan)).getText() + "");
            double interest = Double.parseDouble(((EditText) findViewById(R.id.interestRate)).getText() + "");

            lc.setLoanAmount(amount);
            lc.setNumberOfYears(years);
            lc.setYearlyInterestRate(interest);

            double monthlyPayment = lc.getMonthlyPayment();
            double totalPayment = lc.getTotalCostOfLoan();
            double totalInterest = lc.getTotalInterest();

            ((TextView) findViewById(R.id.monthlyPayment)).setText(monthlyPayment + "");
            ((TextView) findViewById(R.id.totalPayment)).setText(totalPayment + "");
            ((TextView) findViewById(R.id.totalInterest)).setText(totalInterest + "");
        } else {
            Toast.makeText(this, "SOME FIELDS ARE EMPTY YOU FUCK.", Toast.LENGTH_LONG).show();
        }
    }

    public void clear (View view) {
        ((TextView)findViewById(R.id.monthlyPayment)).setText("");
        ((TextView)findViewById(R.id.totalPayment)).setText("");
        ((TextView)findViewById(R.id.totalInterest)).setText("");
        ((EditText)findViewById(R.id.loan)).setText("");
        ((EditText)findViewById(R.id.interestYears)).setText("");
        ((EditText)findViewById(R.id.interestRate)).setText("");
    }
}
